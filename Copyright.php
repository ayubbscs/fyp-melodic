<?php include 'header.php';?>
<?php include 'includes/db_connection.php'; ?>
<?php include('includes/session.php');?>

<div id="fh5co-page">
        <div id="fh5co-intro-section">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 animate-box text-center">
						<h2 class="intro-heading">Melodic "Spread your Voice"</h2>
						<p><span>Created with <i class="icon-heart3"></i> by HAMZA , AYUB &amp; FAISAL  </span></p>
                        <a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle"><i></i></a>
                        <p><?php echo $login_session; ?></p>
                        <h1 class="text-center" style="color:#539DDB;">Copyright Terms &amp; Conditions </h1>
					</div>
				</div>
			</div>
		</div>
    
    <div id="fh5co-contact-section" ng-app="">
			<div class="container">
				<div class="row">
					<div class="col-md-7 col-md-push-1 animate-box">
						<div class="row">
                            <div class="col-md-7">
                                <div class="form-group">
                                    <p><strong>“Copyright”</strong> is the term used to describe a number of legal rights that exist in original literary, musical, dramatic or artistic works, and in sound recordings, films, broadcasts and other creative works. Under copyright laws, these rights are exclusive to the copyright owner, and enable the copyright owner to control how their work is used and to prevent unauthorised use.
                                    Originally, copyright laws allowed the creator of a work to prevent that work from being copied, but copyright laws have gradually been extended over time, and now allow copyright owners to prevent and control things like adaptation or public performance of the copyright work, inclusion of the work in a broadcast, or distribution of the work both physically and on the Internet. Because these rights are exclusive to the copyright owner, anyone wanting to do any of these things needs the permission of the copyright owner.
                                    </p>            
                                </div>
				            </div><br/>&nbsp;
                        </div>
					</div>
				</div>
			</div>
		</div>
        
<?php include 'footer.php';?>    
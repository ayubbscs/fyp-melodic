
<nav id="fh5co-main-nav" role="navigation">
		<a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle active"><i></i></a>
		<div class="js-fullheight fh5co-table">
			<div class="fh5co-table-cell js-fullheight">
				<h1 class="text-center"><a class="fh5co-logo" href="main.php">MELODIC</a></h1>
				<ul>
					<li><a href="discover.php">Discover</a></li>
					<li><a href="GigNews.php">Top Gigs/Events</a></li>
					<li><a href="newsupload.php">Upload News</a></li>
					<li><a href="songupload.php">Upload Song</a></li>
                    <li><a href="userView.php">Profile</a></li>
                    <li><a href="playlist.php">Playlist</a></li>
                    <li><a href="mostRated.php">Most Rated</a></li>
                    <li><a href="updateUser.php">Update Profile</a></li>
                    <li><a href="includes/UserSignOut.php">Sign Out</a></li>
				</ul>
                
				<p class="fh5co-social-icon">
					<a href="#"><i class="icon-twitter2"></i></a>
					<a href="#"><i class="icon-facebook2"></i></a>
					<a href="#"><i class="icon-instagram"></i></a>
					<a href="#"><i class="icon-youtube"></i></a>
				</p>
			</div>
		</div>
</nav>


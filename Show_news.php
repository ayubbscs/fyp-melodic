<?php include 'header.php';?>
<?php include('includes/db_connection.php'); ?> 
<?php include ('includes/session.php'); ?><!--create database connection-->
<!-- <link rel="stylesheet" href="css/newstable.css"> -->

<div id="fh5co-page">
        <div id="fh5co-intro-section">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 animate-box text-center">
						<h2 class="intro-heading">Melodic "A Music Site"</h2>
						<p><span>Created with <i class="icon-heart3"></i> by HAMZA , AYUB &amp; FAISAL  </span></p>
                        <a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle"><i></i></a>
                        <p><?php echo $login_session; ?></p>
                        <h1 class="text-center" style="color:#539DDB;">My News</h1>
					</div>
				</div>
			</div>
		</div>
    

   <div id="demo" >
  <!-- Responsive table starts here -->
  <!-- For correct display on small screens you must add 'data-title' to each 'td' in your table -->
  <div class="table-responsive-vertical shadow-z-1">
  <!-- Table starts here -->
  <table id="table" class="table table-hover table-mc-light-blue">
      <thead>
        <tr class="btn-primary">
          <th>#</th>
          <th>Title</th>
          <th>Description</th>
          <th>Start date</th>
          <th>End date</th>
          <th>Status</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        <?php 
          $que = "SELECT id FROM user_info WHERE Username='$login_session'";
           $result = mysqli_query($conn,$que);
           $row = mysqli_fetch_array($result);
           $Uid = $row['id'];
          
                $query = "SELECT * FROM news WHERE id = $Uid";
                $run = mysqli_query($conn,$query);

                while($row = mysqli_fetch_array($run)){
                    echo '<tr>';
                    echo '<td>'.$row['News_ID'].'</td>';
                    echo '<td>'.$row['News_title'].'</td>';
                    echo '<td>'.$row['News_description'].'</td>';
                    echo '<td>'.$row['Start_date'].'</td>';
                    echo '<td>'.$row['End_date'].'</td>';    
                    echo '<td>'.$row['News_status'].'</td>';
                    
                    echo '<td><a href="edit_news.php?edit_id='.$row['News_ID'].'" class="btn btn-primary">EDIT</a>
                              <a href="del_news.php?delete_id='.$row['News_ID'].'" class="btn btn-danger">DELETE</a></td>';
                    echo '</tr>';
                              
                }
            ?>
      </tbody>
    </table>
  </div>
</div>         

<?php include 'footer.php';?>
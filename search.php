<?php include 'header.php';?>
<?php include('includes/session.php');?>
<?php include('includes/db_connection.php'); ?>

  <body onload="javascript:getPlayer();">
<div id="fh5co-page" >
	
        <div id="fh5co-intro-section">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 animate-box text-center">
						<h2 class="intro-heading">Melodic "Play The Moments"</h2>
						<p><span>Created with <i class="icon-heart3"></i> by HAMZA , AYUB &amp; FAISAL</span></p>
                        <p><?php echo $login_session; ?></p>
                        <a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle"><i></i></a>
                        <h1 class="text-left" style="color:#539DDB;">Search Results</h1>
					</div>
				</div>
			</div>
		</div>
    
    <div id="fh5co-contact-section" style="background-image: url();" ng-app="">
			<div class="container">
				<div class="row">
                    <form name="Form" action="" method="get" novalidate>
					    
                                <?php
                                    if(isset($_POST['search']) && !empty($_POST['search']))
                                    {
                                        $search            = $_POST['search'];
                                        
                                        $que = "SELECT * FROM song WHERE Song_artist='$search' OR Song_name='$search' ";
                                        $run = mysqli_query($conn,$que);
                                        while($row = mysqli_fetch_array($run)){ 
                                              $Sid = $row['Song_ID'];
                                              $Snam = $row['Song_name'];
                                              $Sgen = $row['Song_genre']; 
                                              $Sauthor = $row['Song_artist']; 
                                              $Spath = $row['Song_path'];
                                              $Simg = $row['Song_img']; 
                                ?>
                                <div class="col-md-7 col-md-push-1 animate-box">
                                    <div class="row">
                                        <div id="<?php echo $Sid; ?>" class="aplayer"></div> 
                                        </div>

                                </div><br>&nbsp;
                                    <?php } ?>
                                <?php } ?>        
                    </form>    
                </div>
            </div>
        </div>
    </div>
    
    
        <script>
  function getPlayer() {
      
    
        <?php
      
            $search            = $_POST['search'];
        
        $que = "SELECT * FROM song WHERE Song_artist='$search' OR Song_name='$search'";
        $run = mysqli_query($conn,$que);
            while($row = mysqli_fetch_array($run)){
                $Sid = $row['Song_ID'];
                $Snam = $row['Song_name'];
                $Sgen = $row['Song_genre']; 
                $Sauthor = $row['Song_artist']; 
                $Spath = $row['Song_path'];
                $Simg = $row['Song_img']; 
        ?>
    var ap1 = new APlayer({ element: document.getElementById('<?php echo $Sid; ?>'),
    narrow: false,
    autoplay: false,
    showlrc: false,
    mutex: true,
    theme: '#e6d0b2',
    preload: 'metadata',
    mode: 'circulation',
    music: {
        title: '<?php echo $Snam; ?>',
        author: '<?php echo $Sauthor; ?>',
        url: 'http://localhost/site/songs/<?php echo $Spath; ?>',
        pic: 'http://localhost/site/upload_Image/<?php echo $Simg; ?>'
    }
    });
      <?php } ?>
      
        ap1.on('play', function () {
        console.log('play');
        });
        ap1.on('play', function () {
            console.log('play play');
        });
        ap1.on('pause', function () {
            console.log('pause');
        });
        ap1.on('canplay', function () {
            console.log('canplay');
        });
        ap1.on('playing', function () {
            console.log('playing');
        });
        ap1.on('ended', function () {
            console.log('ended');
        });
        ap1.on('error', function () {
            console.log('error');
        });

  }
            
    </script>

<?php include 'footer.php';?>
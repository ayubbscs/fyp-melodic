<footer>
			<div id="footer">
				<div class="container">
					<div class="row">
						<div class="col-md-3">
							<h3 class="section-title">About Melodic</h3>
							<p>Our plan is to bring a single platform on which music along with updates of different musical events are available.</p>
						</div>
						
						<div class="col-md-3 col-md-push-1">
							<h3 class="section-title">Links</h3>
							<ul>
								<li><a href="discover.php">Discover</a></li>
								<li><a href="songupload.php">Upload song</a></li>
								<li><a href="newsupload.php">Upload News</a></li>
								<li><a href="mostRated.php">Rated Tracks</a></li>
								<li><a href="#">Download App</a></li>
							</ul>
						</div>

						<div class="col-md-3">
							<h3 class="section-title">Information</h3>
							<ul>
                                <li><a href="Copyright.php">Copyright Policy</a></li>
								<li><a href="privacyLegal.php">Privacy &amp; Policy</a></li>
                                <li><a href="cook.php">Cookies Policy</a></li>
								<li><a href="about.php">About Us</a></li>
							</ul>
						</div>
						<div class="col-md-3">
							<h3 class="section-title">Get Updated</h3>
							<p>Subscribe Us to get latest Gigs/Events news right away !!</p>
							<form class="form-inline" id="fh5co-header-subscribe">
								
							</form>
						</div>
					</div>
					<div class="row copy-right">
						<div class="col-md-6 col-md-offset-3 text-center">
							<p class="fh5co-social-icon">
								<a href="#"><i class="icon-twitter2"></i></a>
								<a href="#"><i class="icon-facebook2"></i></a>
								<a href="#"><i class="icon-instagram"></i></a>
								<a href="#"><i class="icon-dribbble2"></i></a>
								<a href="#"><i class="icon-youtube"></i></a>
							</p>
							<p>Copyright 2017 <a href="#">Melodic</a>. All Rights Reserved. <br>Made with <i class="icon-heart3"></i> by <a href="http://freehtml5.co/" target="_blank">Hamza,Ayub &amp; Faisal</a> 
						</div>
					</div>
				</div>
			</div>
    
		</footer>
	
	

	<!-- jQuery -->
<!--	<script src="js/APlayer.js"></script>-->
	<script src="js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="js/jquery.waypoints.min.js"></script>
	<!-- Counters -->
	<script src="js/jquery.countTo.js"></script>
	<!-- Flexslider -->
	<script src="js/jquery.flexslider-min.js"></script>

	<!-- Main JS (Do not remove) -->
	<script src="js/main.js"></script>



    <script src="/site/dist/APlayer.min.js"></script>
    <script src="/site/demo.js"></script>

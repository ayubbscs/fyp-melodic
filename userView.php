<?php include 'header.php';?>
<?php include('includes/session.php');?>


    <div id="fh5co-page">
        <div id="fh5co-intro-section">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 animate-box text-center">
						<h2 class="intro-heading" style="color:#539DDB;">Melodic "A Music Site"</h2>
						<p><span>Created with <i class="icon-heart3"></i> by HAMZA , AYUB &amp; FAISAL  </span></p>
                        <a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle"><i></i></a>
                        <h1 class="text-center" style="color:#539DDB;">Hi <?php echo $login_session; ?></h1>
					</div>
				</div>
			</div>
		</div>
    <div id="fh5co-contact-section" ng-app="">
			<div class="container">
				<div class="row" >
					<div class="col-md-7 col-md-push-1 animate-box">
						<div class="row">
                            <form name="myForm" action="" method="get" novalidate>   
                                  <div id="fh5co-featured">
			                         <div class="container-fluid">
				                        <div class="row">
					                       <div class="post-entry">
						                      <div class="col-md-6 col-md-push-6 nopaddingbottom animate-box" align="right">
                                                  <ul class="fh5co-social-icons">
								                    
								                    <br><li><a href="Show_Song.php">My Songs</a></li>
                                                    <br><li><a href="playlist.php">My Playlist</a></li>  
								                    <br><li><a href="Show_news.php">My News</a></li>
                                                    <br><li><a href="updateUser.php">Update Profile</a></li>
                                                    <br><li><a href="del_user.php">Delete Profile</a></li>
                                                    <br><li><a href="includes/UserSignOut.php">Sign Out</a></li>  
                                                    
							                     </ul><br><br><br><br><br><br><br><br><br><br><br><br>
                                                  <div class="row copy-right">
                                                    <div class="col-md-6 col-md-offset-3 text-center">
                                                        <p class="fh5co-social-icon"> Share Profile on :
                                                            <a href="#"><i class="icon-twitter2"></i></a>
                                                            <a href="#"><i class="icon-facebook2"></i></a>
                                                            <a href="#"><i class="icon-instagram"></i></a>
                                                        </p>
                                                    </div>
					                               </div>
						                      </div>
                                               <?php 
                                                    $query = "SELECT U_name,U_dob,U_image,U_gender,U_address FROM user_info WHERE Username='$login_session'";
                                                    $run = mysqli_query($conn,$query);

                                                    while($row = mysqli_fetch_array($run)){
                                                        echo '<div class="col-md-6 col-md-pull-5 animate-box">';
                                                            echo'<div class="display-tc">';
                                                                echo'<div class="desc">';
                                                                    echo '<img class="img-responsive" src="upload_Image/'.$row['U_image'].'" alt="Image Preview">';
                                                                    echo '<h3>'.$row['U_name'].'</h3>';
                                                                    echo '<strong class="role">Official User</strong>';
                                                                    echo '<p>Following : 3</p>';    
                                                                    echo '<p>Followers : 10</p>';
                                                                    echo '<p>Date of Birth: '.$row['U_dob'].'</p>';
                                                                    echo '<p>Address: '.$row['U_address'].'</p>';
                                                                    echo '<p>Gender: '.$row['U_gender'].'</p>';
                                                                echo'</div>';
                                                            echo'</div>';
                                                        echo'</div>';
                                                    }
                                                ?>	
                                            </div>
					                     </div>
				                     </div>
			                      </div>    
                            </form>
						</div>
					</div>
				</div>
			</div>
        
    
<?php include 'footer.php';?>
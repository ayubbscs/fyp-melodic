<?php include 'header.php';?>
<?php include 'includes/db_connection.php'; ?>
<?php include('includes/session.php');?>

<div id="fh5co-page">
        <div id="fh5co-intro-section">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 animate-box text-center">
						<h2 class="intro-heading">Melodic "Spread your Voice"</h2>
						<p><span>Created with <i class="icon-heart3"></i> by HAMZA , AYUB &amp; FAISAL  </span></p>
                        <a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle"><i></i></a>
                        <p><?php echo $login_session; ?></p>
                        <h1 class="text-center" style="color:#539DDB;">Privacy &amp; Legal Policy</h1>
					</div>
				</div>
			</div>
		</div>
    
    <div id="fh5co-contact-section" ng-app="">
			<div class="container">
				<div class="row">
					<div class="col-md-7 col-md-push-1 animate-box">
						<div class="row">
                            <div class="col-md-7">
                                <div class="form-group">
                                    <p><strong>The</strong> Platform is a hosting service. Registered users of the Platform may submit  (“Content”), which will be stored by Melodic at the direction of such registered users, and may be shared and distributed by such registered users, and other users of the Platform, using the tools and features provided as part of the Platform and accessible via the System. The Platform also enables registered users to interact with one another and to contribute to discussions, and enables any user of the Website, Apps or certain Services (who may or may not be registered users of the Platform) to view, listen to and share Content uploaded and made available by registered users.
                                    We may, from time to time, release new tools and resources on the Website, release new versions of our Apps, or introduce other services and/or features for the Platform. Any new services and features will be subject to these Terms of Use as well as any additional terms of use that we may release for those specific services or features.
                                    </p> 
                                    <h2 style="color:#539DDB;">Privacy says :</h2>
                                    <p><strong>By</strong> using the Website, the Apps or any of the Services, and in particular by registering a Melodic® account, you are consenting to the use of your information in the manner set out in this Privacy Policy.For your convenience, information relating to our use of cookies and similar technologies is set out in a separate Cookies Policy. The Cookies Policy forms part of the Privacy Policy, and whenever we refer to the Privacy Policy, we are referring to the Privacy Policy incorporating the Cookies Policy.
                                    Please take some time to read this Privacy Policy (including the Cookies Policy), and make sure you are happy with our use and disclosure of your information.
                                    If you do not agree to any of the provisions of this Privacy Policy, you should not use the Website, the Apps or any of the Services. 
                                    Please note that this Privacy Policy only applies to the Website. When using the Platform, you may find links to other websites, apps and services, or tools that enable you to share information with other websites, apps and services. Melodic is not responsible for the privacy practices of these other websites, apps and services and we recommend that you review the privacy policies of each of these websites, apps or services before connecting your Melodic® account or sharing any personal data.
                                    </p> 
                                </div>
				            </div><br/>&nbsp;
                        </div>
					</div>
				</div>
			</div>
		</div>
        
<?php include 'footer.php';?>    
<?php include 'header.php';?>
<?php include 'includes/db_connection.php'; ?>
<?php include('includes/session.php');?>
    <?php
            if(isset($_GET['msg']) && !empty($_GET['msg'])){
                echo "<script> swal('Copyright Detected', 'Song already exsist !!', 'error'); </script>";  
            } 
        ?>
    <div id="fh5co-page">
        <div id="fh5co-intro-section">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 animate-box text-center">
						<h2 class="intro-heading">Melodic "When wOrdS Fade, Music SpeakS "</h2>
						<p><span>Created with <i class="icon-heart3"></i> by HAMZA , AYUB &amp; FAISAL  </span></p>
                        <a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle"><i></i></a>
                        <p><?php echo $login_session; ?></p>
                        <h1 class="text-center" style="color:#539DDB;">Upload Song</h1>
					</div>
				</div>
			</div>
		</div>
    <div id="fh5co-contact-section" ng-app="">
			<div class="container">
				<div class="row">
					<div class="col-md-7 col-md-push-1 animate-box">
						<div class="row">
                            <form name="sngUpForm" action="includes/songUp.php" method="post" enctype="multipart/form-data" novalidate>
                                <div class="progress-bar blue shine stripes">
                                    <span style="width: 75%"></span>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="file" value="Choose a file to Upload" onchange="readURL(this);" name="imgfile" id="imgfile" class="inputfile" />
                                        <img class="img-song" id="blah" src="#" alt="your image" >
                                        <br/>&nbsp;
                                    </div>
                                </div>    
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Title" id="title" name="title" ng-model="title" required>
                                        <span ng-show="sngUpForm.title.$touched && sngUpForm.title.$invalid">*Title is Required</span> 
                                    </div>
                                </div><br/>&nbsp;
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Genre" id="genre" name="genre" ng-model="genre" required>
                                        <span ng-show="sngUpForm.genre.$touched && sngUpForm.genre.$invalid">*Genre is Required</span> 
                                    </div>
                                </div><br/>
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Artist Name" id="artist" name="artist" ng-model="artist" required>
                                        <span ng-show="sngUpForm.artist.$touched && sngUpForm.artist.$invalid">*Artist name is Required</span> 
                                    </div>
                                </div><br/><br/><br/><br/>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <textarea name="descript" class="form-control" id="descript" cols="30" rows="7" placeholder="Description"></textarea>
                                    </div>
                                </div><br/><br/>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <p>Choose mp3,ogg,mpeg file to Upload:&nbsp;&nbsp;&nbsp;&nbsp;</p>
                                        <input type="file" value="Choose mp3,ogg,mpeg file to Upload" name="songfile" id="songfile" class="inputfile" />
                                        <br/>
                                    </div>
                                </div>    
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="submit" value="Save" name="submit" ng-disabled="sngUpForm.$invalid" class="btn btn-primary">
                                    </div>
                                </div>
                            </form>
						</div>
					</div>
				</div>
			</div>
		</div>
<?php include 'footer.php';?>
<script>
    function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result)
                        .width(150)
                        .height(200);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
</script>    
<?php include 'header.php';?>
<?php include 'includes/db_connection.php'; ?>
<?php include('includes/session.php');?>

    <div id="fh5co-page">
        <div id="fh5co-intro-section">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 animate-box text-center">
						<h2 class="intro-heading">Melodic "Spread your Voice"</h2>
						<p><span>Created with <i class="icon-heart3"></i> by HAMZA , AYUB &amp; FAISAL  </span></p>
                         <p><?php echo $login_session; ?></p>
                        <a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle"><i></i></a>
                        <h1 class="text-center" style="color:#539DDB;">Edit Gig News</h1>
					</div>
				</div>
			</div>
		</div>
    <?php
        if(isset($_GET['edit_id'])) {
        $id = $_GET['edit_id'];
      }

      $que = "SELECT * FROM news WHERE News_ID = '$id'";
      $run = mysqli_query($conn,$que);
      $row = mysqli_fetch_array($run);
      $Nid = $row['News_ID'];
      $Ntitle = $row['News_title'];
      $Ndes = $row['News_description'];  
      $Nstart = $row['Start_date'];
      $Nend = $row['End_date'];       
      $Nstatus = $row['News_status'];       
    ?>    
        
    <div id="fh5co-contact-section" ng-app="">
			<div class="container">
				<div class="row">
					<div class="col-md-7 col-md-push-1 animate-box">
						<div class="row">
                            <form name="newsForm" action="includes/news_update.php" method="post" enctype="multipart/form-data" novalidate>
                                 <div class="col-md-7">
								    <div class="form-group">
									   <input type="hidden" name="Newsid" value="<?php echo $Nid; ?>" class="form-control" id="Newsid"  placeholder="Newsid" >
								    </div>
							     </div><br/>&nbsp;
                                
							     <div class="col-md-7">
								    <div class="form-group">
									   <input type="text" name="title" value="<?php echo $Ntitle; ?>" class="form-control" id="title"  placeholder="Title">
								    </div>
							     </div><br/>&nbsp;
							     <div class="col-md-12">
								    <div class="form-group">
									   <textarea name="descript" class="form-control" id="descript"  cols="30" rows="7" placeholder="Description"><?php echo $Ndes; ?>"</textarea>
								    </div>
							     </div><br/><br/>
                                 
                                 <div class="col-md-12">
                                    <div class="form-group">
                                        <p>Previous Start date:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $Nstart; ?></p>
                                    </div>
							     </div><br/><br/>
                                
                                 <div class="col-md-12">
                                    <div class="form-group">
                                        <p>Update Start date:</p>
                                        <input type="date" name="startdate" id="startdate" value="" min="2017-01-01" ng-model="startdate" required><br/>
                                          <span ng-show="newsForm.startdate.$touched"></span>     
                                          <span class="help-block" ng-show="newsForm.startdate.$error.required">*Required!</span>
                                          <span class="help-block" ng-show="newsForm.startdate.$error.date">*Not a valid date!</span>
                                    </div>
							     </div><br/><br/>
                                
                                 <div class="col-md-12">
                                    <div class="form-group">
                                        <p>Previous End date:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $Nend; ?></p>
                                    </div>
							     </div><br/><br/>    
                                
                                 <div class="col-md-12">
                                    <div class="form-group">
                                        <p>Update End date:</p>
                                        <input type="date" name="enddate" id="enddate" max="2030-01-01" value="<?php echo $Nend; ?>" ng-model="enddate" required>
                                          <span ng-show="newsForm.enddate.$touched"></span>     
                                          <span class="help-block" ng-show="newsForm.enddate.$error.required">*Required!</span>
                                          <span class="help-block" ng-show="newsForm.enddate.$error.date">*Not a valid date!</span>
                                    </div>
							     </div><br/><br/>
							     <div class="col-md-12">
								    <div class="form-group">
									   <input type="submit" name="update" value="Update News" ng-disabled="newsForm.$invalid" class="btn btn-primary">
                                        
								    </div>
							     </div>
                                
                            </form>
						</div>
					</div>
				</div>
			</div>
		</div>
        
<?php include 'footer.php';?>
<?php include 'header.php';?>
<?php include 'includes/db_connection.php'; ?>
<?php include('includes/session.php');?>

    <div id="fh5co-page">
        <div id="fh5co-intro-section">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 animate-box text-center">
						<h2 class="intro-heading">Melodic "Spread your Voice"</h2>
						<p><span>Created with <i class="icon-heart3"></i> by HAMZA , AYUB &amp; FAISAL  </span></p>
                        <p><?php echo $login_session; ?></p>
                        <a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle"><i></i></a>
                        <h1 class="text-center" style="color:#539DDB;">Edit Song</h1>
					</div>
				</div>
			</div>
		</div>
    <?php
        if( isset( $_GET['edit_id'])) {
        $id = $_GET['edit_id'];

      }

      $que = "SELECT * FROM song WHERE Song_ID = '$id'";
      $run = mysqli_query($conn,$que);
      $row = mysqli_fetch_array($run);
      $Sid = $row['Song_ID'];
      $Stitle = $row['Song_name'];
      $Sgenre = $row['Song_genre'];    
      $Sdes = $row['Song_description'];  
      $Sartist = $row['Song_artist'];       
    ?>   

        <div id="fh5co-contact-section" ng-app="">
			<div class="container">
				<div class="row">
					<div class="col-md-7 col-md-push-1 animate-box">
						<div class="row">
                            <form name="sngUpForm" action="includes/song_update.php" method="post" enctype="multipart/form-data" novalidate>
                                <div class="col-md-7">
								    <div class="form-group">
									   <input type="hidden" name="Songid" value="<?php echo $Sid; ?>" class="form-control" id="Songid"  placeholder="Songid" >
								    </div>
							     </div><br/>&nbsp;
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="file" value="Choose a file to Upload" onchange="readURL(this);" name="imgfile" id="imgfile" class="inputfile" />
                                        <img class="img-song" id="blah" src="#" alt="your image" >
                                        <br/>&nbsp;
                                    </div>
                                </div>    
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <p>Previous value:&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $Stitle; ?></p>
                                        <input type="text" class="form-control" placeholder="Update Title" value="" id="title" name="title" ng-model="title" required>
                                        <span ng-show="sngUpForm.title.$touched && sngUpForm.title.$invalid">*Title is Required</span> 
                                    </div>
                                </div><br/>&nbsp;
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <p>Previous value:&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $Sgenre; ?></p>
                                        <input type="text" class="form-control" placeholder="Update Genre" id="genre" value="" name="genre" ng-model="genre" required>
                                        <span ng-show="sngUpForm.genre.$touched && sngUpForm.genre.$invalid">*Genre is Required</span> 
                                    </div>
                                </div><br/>
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <p>Previous value:&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $Sartist; ?></p>
                                        <input type="text" class="form-control" placeholder="Update Artist Name" id="artist" value="" name="artist" ng-model="artist" required>
                                        <span ng-show="sngUpForm.artist.$touched && sngUpForm.artist.$invalid">*Artist name is Required</span> 
                                    </div>
                                </div><br/><br/><br/><br/>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <textarea name="descript" class="form-control" id="descript" cols="30" rows="7" placeholder="Description"><?php echo $Sdes; ?></textarea>
                                    </div>
                                </div><br/><br/>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <p>Choose mp3,ogg,mpeg file to Upload:&nbsp;&nbsp;&nbsp;&nbsp;</p>
                                        <input type="file" value="Choose mp3,ogg,mpeg file to Upload" name="songfile" id="songfile" class="inputfile" />
                                        <br/>
                                    </div>
                                </div>    
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="submit" value="Update Song" name="update" ng-disabled="sngUpForm.$invalid" class="btn btn-primary">
                                    </div>
                                </div>
                            </form>
						</div>
					</div>
				</div>
			</div>
		</div>
        
        
<?php include 'footer.php';?>  
<script>
    function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result)
                        .width(150)
                        .height(200);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
</script>            
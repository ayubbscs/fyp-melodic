        <header>
            <head><meta name="viewport" content="width=device-width, initial-scale=1">
                <?php include('includes/db_connection.php'); ?>
            
    <link rel="stylesheet" href="https://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css">
    <script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="https://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>
    <style> 
        input[type=text] {
            border: 2px solid #0056e2;
            border-radius: 4px;
            font-size: 16px;
            background-color: black;
            background-position: 10px 10px; 
            padding: 12px 20px 12px 40px;
            -webkit-transition: width 0.4s ease-in-out;
            transition: width 0.4s ease-in-out;
        }
        .dropdown {
            display:inline-block;
            margin-left:20px;
            padding:10px;
        }
        .glyphicon-bell {
            font-size:1.5rem;
        }
        .notifications {
            min-width:420px; 
        }
        .notifications-wrapper {
            overflow:auto;
            max-height:250px;
        }
        .menu-title {
            color:#6aa2fc;
            font-size:1.5rem;
            display:inline-block;
        }
        .glyphicon-circle-arrow-right {
            margin-left:10px;     
        }
        .notification-heading, .notification-footer  {
 	        padding:2px 10px;
        }
        .dropdown-menu.divider {
            margin:5px 0;          
        }
        .item-title {
            font-size:1.3rem;
            color:#000;
        }
        .notifications a.content {
            text-decoration:none;
            background:#ccc;
        }
        .notification-item {
            padding:10px;
            margin:5px;
            background:#ccc;
            border-radius:4px;
        }
    </style>
                <script>
                    document.onkeydown=function(evt){
                    var keyCode = evt ? (evt.which ? evt.which : evt.keyCode) : event.keyCode;
                        if(keyCode == 13)
                        {
                            //your function call here
                            document.test.submit();
                        }
                    }
                </script>
            </head>
            
			<div class="container">
				<div class="fh5co-navbar-brand">
					<div class="row">
						<div class="col-xs-6">                        
							<h1 class="text-left"><a class="fh5co-logo"> MELODIC <span>"A Music Site" </span></a></h1>
                            
                            
                            <form method="post" action="search.php" name="test">
                                <input id="search" name="search" type="text"  placeholder="Looking for Tracks !!! ..." />
                                
                            </form>
                                	
                            <div class="dropdown">
                                <a id="dLabel" role="button" data-toggle="dropdown" data-target="#" href="/page.html">
                                <i class="glyphicon glyphicon-bell"></i>
                                </a>
  
                              <ul class="dropdown-menu notifications" role="menu" aria-labelledby="dLabel">
    
                                <div class="notification-heading"><h4 class="menu-title">Notifications</h4></div>
                                <li class="divider"></li>
                                <div class="notifications-wrapper">
                                    <?php 
                                       $que = "SELECT id FROM user_info WHERE Username='$login_session'";
                                       $result = mysqli_query($conn,$que);
                                       $row = mysqli_fetch_array($result);
                                       $Uid = $row['id'];

                                        $query = "SELECT * FROM news WHERE id = $Uid  && News_status='Approved'";
                                        $run = mysqli_query($conn,$query);

                                        while($row = mysqli_fetch_array($run)){
                                            echo '<a class="content" href="#">';
                                                echo'<div class="notification-item">';                
                                                    echo '<h4 class="item-title">'.$row['News_title'].'</h4>';
                                                    echo '<p class="item-info">'.$row['News_status'].'</p>';
                                                echo '</div>';
                                            echo '</a>';
                                        }
                                    ?>
                                </div>
    
                              </ul>
                            </div>
						</div>
                        
						<div class="col-xs-6">
                            <p class="fh5co-social-icon text-right">
								<?php echo $login_session; ?>
							</p>
						</div>
                        
					</div>
					<a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle"><i></i></a>
				</div>
			</div>
            
		</header>
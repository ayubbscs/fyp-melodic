-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 05, 2017 at 07:21 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `melodic_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_info`
--

CREATE TABLE `admin_info` (
  `Admin_ID` int(20) NOT NULL,
  `Admin_name` varchar(30) NOT NULL,
  `A_password` varchar(100) NOT NULL,
  `Admin_dob` date NOT NULL,
  `Admin_gender` text NOT NULL,
  `Admin_address` varchar(50) NOT NULL,
  `Admin_photo` varchar(100) NOT NULL,
  `Admin_headerphoto` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_info`
--

INSERT INTO `admin_info` (`Admin_ID`, `Admin_name`, `A_password`, `Admin_dob`, `Admin_gender`, `Admin_address`, `Admin_photo`, `Admin_headerphoto`) VALUES
(0, 'Ayub', 'bcs01233330@', '1995-01-29', 'Male', 'Lawrence Road', 'k26217574.jpg', ''),
(1, 'Tariq', '123456', '2017-03-23', 'Male', 'asdnlajf', 'kajsdajkshfl', '');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `News_ID` int(10) NOT NULL,
  `News_title` varchar(30) NOT NULL,
  `News_description` varchar(200) NOT NULL,
  `Start_date` date NOT NULL,
  `End_date` date NOT NULL,
  `News_status` varchar(30) NOT NULL,
  `Admin_ID` int(20) DEFAULT NULL,
  `id` int(11) DEFAULT NULL,
  `mobile` bigint(50) NOT NULL,
  `cnic` bigint(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`News_ID`, `News_title`, `News_description`, `Start_date`, `End_date`, `News_status`, `Admin_ID`, `id`, `mobile`, `cnic`) VALUES
(14, 'UOL Concert', 'Organized by AYT or Tiwana Group.\r\nSinger Atif Aslam\"', '2017-06-27', '2017-06-27', 'Approved', 1, 23, 320410467, 912312341),
(15, 'amadeus live with VPAC!', 'Location:  Valley Performing Arts Center Great Hall\r\nRichard Kaufman conductor \r\nLos Angeles Chamber Orchestra\r\nMembers of LA Opera Chorus', '2017-05-29', '2017-05-30', 'Unapproved', 1, 10, 3204104607, 32465131231),
(16, 'Free Community Concert', 'Location: California Plaza Water Court\r\nCarlos Izcaray conductor\r\nKevin Miura violin\r\nPresented in partnership with The Colburn School and Grand Performances.', '2017-07-03', '2017-07-05', 'Unapproved', 1, 10, 0, 0),
(17, 'Drab Majesty', 'Location: Marble Bar Detroit, ML, US\r\nTickets available at Marble Bar ', '2017-04-04', '2017-04-04', 'Unapproved', 1, 23, 0, 0),
(18, 'Mac DeMarco Concert', 'Location: Ace Hotel, Chicago, IL, US', '2017-07-19', '2017-07-20', 'Unapproved', 1, 23, 0, 0),
(19, 'UOL COncert ', 'Location: Auditorium 2 \r\nTickets: --', '2018-12-03', '2019-01-04', 'Approved', 1, 25, 0, 0),
(20, 'News t83', 'Event at Alhamra \r\nTicket: 400pkr per person\r\n', '2017-09-20', '2017-09-13', 'Unapproved', 1, 16, 3201234121, 23123412323);

-- --------------------------------------------------------

--
-- Table structure for table `playlist`
--

CREATE TABLE `playlist` (
  `playlist_id` int(11) NOT NULL,
  `playlist_songName` varchar(100) NOT NULL,
  `playlist_songPath` varchar(255) NOT NULL,
  `playlist_songImg` varchar(255) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `playlist`
--

INSERT INTO `playlist` (`playlist_id`, `playlist_songName`, `playlist_songPath`, `playlist_songImg`, `id`) VALUES
(0, '', ' ', '', 15),
(6, 'Talk ya Ear', '01-Chris Brown - Talk Ya Ear Off (Prod. By Timbaland) (www.SongsLover.com).mp3 ', '', 3),
(9, 'Ratchet', '03 - - Ratchet (Feat Kreayshawn).mp3 ', '25951_679424298752135_2127412756_n.jpg', 3),
(12, 'A year Without Rain', 'a year without rain-salena gomez.mp3 ', '25951_679424298752135_2127412756_n.jpg', 3),
(29, 'Romantic Mashup', '02 Pop Danthology 2015 (Part 2).mp3 ', '', 3);

-- --------------------------------------------------------

--
-- Table structure for table `song`
--

CREATE TABLE `song` (
  `Song_ID` int(10) NOT NULL,
  `Song_name` varchar(30) NOT NULL,
  `Song_genre` varchar(20) NOT NULL,
  `Song_description` varchar(265) NOT NULL,
  `Song_artist` varchar(20) NOT NULL,
  `Song_likes` int(10) NOT NULL,
  `Admin_ID` int(10) NOT NULL,
  `id` int(11) NOT NULL,
  `Song_path` varchar(255) NOT NULL,
  `Song_img` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `song`
--

INSERT INTO `song` (`Song_ID`, `Song_name`, `Song_genre`, `Song_description`, `Song_artist`, `Song_likes`, `Admin_ID`, `id`, `Song_path`, `Song_img`) VALUES
(15, 'Pop Danthology 3 ', 'pop', '', 'Daniel kim', 0, 1, 9, '02 Pop Danthology 2015 (Part 2).mp3', ''),
(24, 'Million Miles ', 'Pop ', '', 'Kreawshwan', 0, 1, 22, '03 - - Ratchet (Feat Kreayshawn).mp3', ''),
(27, 'Hawayein', 'Romantic', '', 'Arjit', 0, 1, 22, '02 - Heartless - Main Dhoondne Ko Zamaane Mein.mp3', 'IMG_1188.JPG');

-- --------------------------------------------------------

--
-- Table structure for table `user_info`
--

CREATE TABLE `user_info` (
  `id` int(11) NOT NULL,
  `Username` varchar(100) NOT NULL,
  `U_name` text NOT NULL,
  `U_password` varchar(100) NOT NULL,
  `U_email` varchar(100) NOT NULL,
  `U_num` bigint(50) NOT NULL,
  `U_address` varchar(200) NOT NULL,
  `U_gender` text NOT NULL,
  `U_dob` date NOT NULL,
  `U_image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_info`
--

INSERT INTO `user_info` (`id`, `Username`, `U_name`, `U_password`, `U_email`, `U_num`, `U_address`, `U_gender`, `U_dob`, `U_image`) VALUES
(8, 'adad88', 'Ayub', '128081', 'azharakhterkhan@gmail.com', 2147483647, 'Pindi Stop', 'Male', '2007-02-01', 'portrait-photography-christian-boecker-aachen-editorial-angelina-petrova-4155.jpg'),
(9, 'asda12', 'Ayoub', '123', 'ayyubyoyo@hotmail.com', 3204104607, 'Near Mayo Hospital', 'Male', '2017-05-04', 'portrait-photography-christian-boecker-aachen-editorial-angelina-petrova-4155.jpg'),
(10, 'yasir', 'Yasir Sherwani', '123', 'yasir@gmail.com', 3204104607, 'Lahore', 'Male', '2017-05-26', 'family collage 2nd.png'),
(11, 'Hamza123', 'Hamza saleem', '123', 'hamza@gmail.com', 12399131312, 'Allama Iqbal Town', 'Male', '2014-02-01', 'IMG_4611.JPG'),
(13, 'buddy123', 'Tariq', 'bcs02133330@', 'ayyubyoyo@hotmail.com', 32145602341, 'Valencia', 'Others', '2017-04-05', '19859342_297402630722262_12739263_o.jpg'),
(15, 'Naeem12', 'Naeem khan', '123', 'm@gmail.com', 3204104607, 'DHA', '', '2017-05-30', ''),
(16, 'Ali123', 'Ali', 'ali@1234', 'ali@gmail.com', 32, 'gawamandi', '', '2017-05-04', ''),
(17, 'hamza123', 'Hamza', '123@123ay', 'hamza@gmail.com', 12345671231, 'gawamandi', 'Male', '2017-04-04', 'k26217574.jpg'),
(18, 'T832', 'Anushka Sharma ', 'bcs02133330@', 'ayub@fashiony.pk', 3204104607, 'DHA Block 2F', 'Male', '2017-05-30', 'Zhalay-Sarhadi-Biography.jpg'),
(19, 'yoyo', 'Anam tariq', 'asid@123', 'ayyubyoyo@hotmail.com', 1239124123, 'Green Town', 'Male', '2017-05-28', 'Zhalay-Sarhadi-Recent-Bold-Photo-Shoot.jpg'),
(20, 'T80', 'Adnan ', 'bcs02133302@', 'ayyubyoyo@hotmail.com', 1230910231, 'Wapda Town', 'Male', '2017-05-29', 'Zhalay-Sarhadi-Biography.jpg'),
(21, 'TT23', 'Zoha khan', 'bcs02133330@', 'ada444.as@gmail.com', 21239101231, 'Allama Iqbal Town', 'Female', '2017-05-28', 'Zhalay-Sarhadi-Biography.jpg'),
(22, 'Zainab123', 'Zainab Idrees', 'zaino@123', 'ZainabPISJ@hotmail.com', 3240123812, 'Lawrence road Lahore', 'Female', '1997-11-06', 'Zhalay-Sarhadi-Biography.jpg'),
(23, 'Faisal12', 'Faisal Sheikh', 'bcs02133330@', 'faisal@gmail.com', 3244901516, 'Johar town', 'Male', '2017-05-29', ''),
(24, 'Ayub24', 'Ayub Idrees', 'bcs02133330@', 'ayubbuddy@gmail.com', 3204104607, 'Lawrence Road', 'Male', '2017-03-29', '1947972_659199937472459_1690654775_n.jpg'),
(25, 'Faisal23', 'Faisal sheikh', '12345a@a', 'ayub@gmail.pk', 3224607123, '', 'Male', '2017-04-05', '7866_217601498403381_2054402884_n.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `user_login`
--

CREATE TABLE `user_login` (
  `User_ID` int(10) NOT NULL,
  `UserName` int(20) NOT NULL,
  `User_password` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_login`
--

INSERT INTO `user_login` (`User_ID`, `UserName`, `User_password`) VALUES
(1, 0, 123),
(2, 0, 0),
(3, 0, 0),
(4, 0, 1234),
(5, 0, 123),
(6, 0, 123),
(7, 0, 123);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_info`
--
ALTER TABLE `admin_info`
  ADD PRIMARY KEY (`Admin_ID`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`News_ID`),
  ADD KEY `admin_news_fk` (`Admin_ID`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `playlist`
--
ALTER TABLE `playlist`
  ADD PRIMARY KEY (`playlist_id`);

--
-- Indexes for table `song`
--
ALTER TABLE `song`
  ADD PRIMARY KEY (`Song_ID`),
  ADD KEY `admin_song_fk` (`Admin_ID`),
  ADD KEY `user_song_fk` (`id`);

--
-- Indexes for table `user_info`
--
ALTER TABLE `user_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_login`
--
ALTER TABLE `user_login`
  ADD PRIMARY KEY (`User_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `News_ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `song`
--
ALTER TABLE `song`
  MODIFY `Song_ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `user_info`
--
ALTER TABLE `user_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `user_login`
--
ALTER TABLE `user_login`
  MODIFY `User_ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `news`
--
ALTER TABLE `news`
  ADD CONSTRAINT `admin_news_fk` FOREIGN KEY (`Admin_ID`) REFERENCES `admin_info` (`Admin_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_news_fk` FOREIGN KEY (`id`) REFERENCES `user_info` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `song`
--
ALTER TABLE `song`
  ADD CONSTRAINT `admin_song_fk` FOREIGN KEY (`Admin_ID`) REFERENCES `admin_info` (`Admin_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_song_fk` FOREIGN KEY (`id`) REFERENCES `user_info` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

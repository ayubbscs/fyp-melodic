<?php include 'header.php';?>
<?php include 'includes/db_connection.php'; ?>
<?php include('includes/session.php');?>
    <div id="fh5co-page">
        <div id="fh5co-intro-section">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 animate-box text-center">
						<h2 class="intro-heading">Melodic "Spread your Voice"</h2>
						<p><span>Created with <i class="icon-heart3"></i> by HAMZA , AYUB &amp; FAISAL  </span></p>
                        <a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle"><i></i></a>
                        <p><?php echo $login_session; ?></p>
                        <h1 class="text-center" style="color:#539DDB;">Upload Gig News</h1>
					</div>
				</div>
			</div>
		</div>
    <div id="fh5co-contact-section" ng-app="">
			<div class="container">
				<div class="row">
					<div class="col-md-7 col-md-push-1 animate-box">
						<div class="row">
                            <form name="newsForm" action="includes/newsUp.php" method="post" enctype="multipart/form-data" novalidate>
							     <div class="col-md-7">
								    <div class="form-group">
									   <input type="text" name="title" class="form-control" id="title"  placeholder="Title">
								    </div>
							     </div><br/>&nbsp;
							     <div class="col-md-12">
								    <div class="form-group">
									   <textarea name="descript" class="form-control" id="descript" cols="30" rows="7" placeholder="Description"></textarea>
								    </div>
							     </div><br/><br/>
                                
                                 <div class="col-md-12">
                                    <div class="form-group">
                                        <p>Start Date:</p>
                                        <input type="date" name="startdate" id="startdate"  min="2017-09-09" ng-model="startdate" required><br/>
                                          <span ng-show="newsForm.startdate.$touched"></span>     
                                          <span class="help-block" ng-show="newsForm.startdate.$error.required">*Required!</span>
                                          <span class="help-block" ng-show="newsForm.startdate.$error.date">*Not a valid date!</span>
                                    </div>
							     </div><br/><br/>
                            
                                 <div class="col-md-12">
                                    <div class="form-group">
                                        <p>End Date:</p>
                                        <input type="date" name="enddate" id="enddate" min="2017-09-09" max="2030-01-01" ng-model="enddate" required>
                                          <span ng-show="newsForm.enddate.$touched"></span>     
                                          <span class="help-block" ng-show="newsForm.enddate.$error.required">*Required!</span>
                                          <span class="help-block" ng-show="newsForm.enddate.$error.date">*Not a valid date!</span>
                                    </div>
							     </div><br/><br/>
                                <p><strong> Enter CNIC &amp; Phone number for verification of news </strong></p>
                                <div class="col-md-7">
								    <div class="form-group">
									   <input type="number" class="form-control" placeholder="Mobile Number" id="M_no" name="M_no" ng-model="mobileNumber" ng-minlength="11" ng-maxlength="11" required>
                                        <span class="help-block" ng-show="myForm.M_no.$error.required || myForm.M_no.$error.number">*Valid phone number is required</span>
                                        <span class="help-block" ng-show="((myForm.M_no.$error.minlength || myForm.M_no.$error.maxlength) && myForm.M_no.$dirty) ">*Phone number should be 11 digits</span>
								    </div>
							     </div><br/><br/>
                                 <div class="col-md-7">
								    <div class="form-group">
									   <input type="number" class="form-control" placeholder="CNIC" id="cnic_no" name="cnic_no" ng-model="cnicNumber" ng-minlength="11" ng-maxlength="11" required>
                                     </div>
                                </div>
                                    
							     <div class="col-md-12">
								    <div class="form-group">
									   <input type="submit" name="submit" value="Submit News" ng-disabled="newsForm.$invalid" class="btn btn-primary">
                                        
								    </div>
							     </div>
                              
                            </form>
						</div>
					</div>
				</div>
			</div>
		</div>
        
<?php include 'footer.php';?>
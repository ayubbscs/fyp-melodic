<?php include 'header.php';?>
<?php include 'includes/db_connection.php'; ?>
<?php include('includes/session.php');?>

<div id="fh5co-page">
        <div id="fh5co-intro-section">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 animate-box text-center">
						<h2 class="intro-heading">Melodic "Spread your Voice"</h2>
						<p><span>Created with <i class="icon-heart3"></i> by HAMZA , AYUB &amp; FAISAL  </span></p>
                        <a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle"><i></i></a>
                        <p><?php echo $login_session; ?></p>
                        <h1 class="text-center" style="color:#539DDB;">About Us</h1>
					</div>
				</div>
			</div>
		</div>
    
    <div id="fh5co-contact-section" ng-app="">
			<div class="container">
				<div class="row">
					<div class="col-md-7 col-md-push-1 animate-box">
						<div class="row">
                            <div class="col-md-7">
                                <div class="form-group">
                                    <p><strong>“ Melodic”</strong> lets people discover and enjoy the greatest selection of music from the most diverse creator community on earth. The platform has become renowned for its unique content and features, including the ability to share talent and connect directly with artists, as well as unearth breakthrough tracks, raw demos, podcasts and more. This is made possible by an open platform that directly connects creators and their fans across the globe. Music and audio creators use Melodic to both share and monetise their content with a global audience, as well as receive detailed stats and feedback from the Melodic community.
                                    </p>            
                                </div>
				            </div><br/>&nbsp;
                        </div>
					</div>
				</div>
			</div>
		</div>
        
<?php include 'footer.php';?>    
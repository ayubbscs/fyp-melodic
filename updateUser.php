<?php include 'header.php';?>
<?php include 'includes/db_connection.php'; ?>
<?php include('includes/session.php');?>

    <div id="fh5co-page">
        <div id="fh5co-intro-section">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 animate-box text-center">
						<h2 class="intro-heading">Melodic "A Music Site"</h2>
						<p><span>Created with <i class="icon-heart3"></i> by HAMZA , AYUB &amp; FAISAL  </span></p>
                        <a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle"><i></i></a>
                        <p><?php echo $login_session; ?></p>
                        <h1 class="text-center" style="color:#539DDB;">Update profile</h1>
					</div>
				</div>
			</div>
		</div>
        <?php
        
      $que = "SELECT * FROM user_info WHERE Username = '$login_session'";
      $run = mysqli_query($conn,$que);
      $row = mysqli_fetch_array($run);
      $Uid = $row['id'];
      $Uname = $row['U_name'];
      $password = $row['U_password'];  
      $Umail = $row['U_email'];
      $num = $row['U_num'];       
      $address = $row['U_address'];       
      $gender = $row['U_gender'];
      $dob = $row['U_dob'];       
                 
    ?>    

        <div id="fh5co-contact-section" ng-app="">
			<div class="container">
				<div class="row">
                    
					<div class="col-md-7 col-md-push-1 animate-box">
						<div class="row">
                            <form name="myForm" action="includes/User_update.php" enctype="multipart/form-data" method="post" novalidate>
                                 <div class="col-md-7">
								    <div class="form-group">
									   <input type="hidden" name="id" value="<?php echo $Uid; ?>" class="form-control" id="id"  placeholder="id" >
								    </div>
							     </div><br/>&nbsp;
                                 <div class="col-md-7">
								    <div class="form-group">
                                        <p>Previous name:&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $Uname; ?></p>
									   <input type="text" class="form-control" placeholder="Update name" value="<?php echo $Uname; ?>" id="Uname" name="Uname" >  
								    </div>
							     </div><br/>&nbsp;
							     
                                <div class="col-md-7">
								    <div class="form-group">
                                        <p>Old Password:&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $password; ?></p>
                                        <input type="password" class="form-control" placeholder="New Password" id="passW" name="passW" value="<?php echo $password; ?>">
								    </div>
							     </div><br/><br/>
							     <div class="col-md-7">
								    <div class="form-group">
                                        <p>Old email:&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $Umail; ?></p>
									   <input type="email" class="form-control" placeholder="New email address" id="email" name="email" value="<?php echo $Umail; ?>" >
								    </div>
							     </div><br/>
                                 <div class="col-md-7">
								    <div class="form-group">
                                        <p>Old number:&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $num; ?></p>
									   <input type="number" class="form-control" placeholder="New Mobile Number" id="M_no" name="M_no" value="<?php echo $num; ?>">
								    </div>
							     </div><br/><br/><br/><br/>
                                 <div class="col-md-7">
								    <div class="form-group">
									   <input type="text" class="form-control" placeholder="Address" value="<?php echo $address; ?>" id="add" name="add">
								    </div>
							     </div><br/><br/>
                                 <div class="col-md-12">
                                    <div class="form-group">
                                        <p>Previous gender:&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $gender; ?></p>
                                        Gender: <br/>
                                        <input type="radio" name="optGender" id="optGender" group="optGender" value="<?php echo $gender; ?>" >Male<br/> 
                                        <input type="radio" name="optGender" id="optGender" group="optGender" value="<?php echo $gender; ?>" >Female<br/> 
                                        <input type="radio" name="optGender" id="optGender" group="optGender" value="<?php echo $gender; ?>" >Others
                                    </div>
							     </div><br/><br/>  
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <p>Previous Date of Birth:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $dob; ?></p>
                                    </div>
							     </div><br/><br/>
                                 <div class="col-md-12">
                                    <div class="form-group">
                                        <p>Update Date of Birth:</p>
                                        <input type="date" id="birthday" name="birthday" value="<?php echo $dob; ?>" ><br/> 
                                    </div>
				                 </div><br/><br/>
                    
                                 <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="file" value="Choose a file to Upload" onchange="readURL(this);" name="image" id="image" class="inputfile" />
                                        <img class="img-song" id="blah" src="#" alt="your image" >
                                        <br/>&nbsp;
                                    </div>
                                </div>
                                
							     <div class="col-md-12">
								    <div class="form-group">
									   <input type="submit" name="submit" value="Update Profile"  class="btn btn-primary">  
								    </div>
							     </div>
                                
                            </form>
						</div>
					</div>
				</div>
			</div>

           
<?php include 'footer.php';?>
<script>
    function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result)
                        .width(150)
                        .height(200);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
</script>             
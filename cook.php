<?php include 'header.php';?>
<?php include 'includes/db_connection.php'; ?>
<?php include('includes/session.php');?>

<div id="fh5co-page">
        <div id="fh5co-intro-section">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 animate-box text-center">
						<h2 class="intro-heading">Melodic "Spread your Voice"</h2>
						<p><span>Created with <i class="icon-heart3"></i> by HAMZA , AYUB &amp; FAISAL  </span></p>
                        <a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle"><i></i></a>
                        <p><?php echo $login_session; ?></p>
                        <h1 class="text-center" style="color:#539DDB;">Cookies Policy</h1>
					</div>
				</div>
			</div>
		</div>
    
    <div id="fh5co-contact-section" ng-app="">
			<div class="container">
				<div class="row">
					<div class="col-md-7 col-md-push-1 animate-box">
						<div class="row">
                            <div class="col-md-7">
                                <div class="form-group">
                                    <p><strong>This Cookies</strong> Policy forms part of our general Privacy Policy.
                                    In common with most other websites, we use cookies and similar technologies to help us understand how people use Melodic® so that we can keep improving our platform. We have created this Cookies Policy to provide you with clear and explicit information about the technologies that we use on Melodic®, and your choices when it comes to these technologies.
                                    If you choose to use the Platform without blocking or disabling cookies or opting out of other technologies, you will indicate your consent to our use of these cookies and other technologies and to our use (in accordance with this policy and the rest of our Privacy Policy) of any personal information that we collect using these technologies. If you do not consent to the use of these technologies, please be sure to block or disable them using your browser settings, the opt-out links identified in this policy.
                                    </p>            
                                </div>
				            </div><br/>&nbsp;
                        </div>
					</div>
				</div>
			</div>
		</div>
        
<?php include 'footer.php';?>    
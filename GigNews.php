<?php include 'header.php';?>
<?php include 'includes/db_connection.php'; ?>
<?php include('includes/session.php');?>
    <div id="fh5co-page">
        <div id="fh5co-intro-section">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 animate-box text-center">
						<h2 class="intro-heading">Melodic "Spread your Voice"</h2>
						<p><span>Created with <i class="icon-heart3"></i> by HAMZA , AYUB &amp; FAISAL  </span></p>
                        <a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle"><i></i></a>
                        <p><?php echo $login_session; ?></p>
                        <h1 class="text-center" style="color:#539DDB;">Event/Gig News</h1>
					</div>
				</div>
			</div>
		</div>
        
    <div id="fh5co-contact-section" ng-app="">
			<div class="container">
				<div class="row">
					<div class="col-md-7 col-md-push-1 animate-box">
						<div class="row">
                            <?php 
                                $query = "SELECT * FROM news";
                                $run = mysqli_query($conn,$query);

                                while($row = mysqli_fetch_array($run)){
                                    echo '<div class="col-md-7">';
                                        echo'<div class="form-group">';                
                                            echo '<h2 style="color:#539DDB;">'.$row['News_title'].'</h2>';
                                            echo '<p>'.$row['News_description'].'</p>';
                                            echo '<span>Event Starts On: </span>&nbsp;'.$row['Start_date'].'<br>';
                                            echo '<span>Event Ends On: </span>'.$row['End_date'].'<br>';    
                                            echo '<span>Officially: </span>'.$row['News_status'];
                                        echo '</div>';
				                    echo '</div><br/>&nbsp;';
                                }
                            ?>	
						</div>
					</div>
				</div>
			</div>
		</div>
        
<?php include 'footer.php';?>
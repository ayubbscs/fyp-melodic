<?php include 'header.php';?>
<?php include('includes/db_connection.php'); ?> <!--create database connection-->
<?php include('includes/session.php');?>

<div id="fh5co-page">
	<?php include 'body1.php';?>
	<?php include 'body2.php'; ?>
    <?php
    if(isset($_GET['msg']) && !empty($_GET['msg'])){
      echo "<script> window.alert('Data Saved'); </script>";  
    } 
    ?>
        <div id="fh5co-intro-section">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 animate-box text-center">
						<h2 class="intro-heading">Melodic "Play The Moments"</h2>
						<p><span>Created with <i class="icon-heart3"></i> by HAMZA , AYUB &amp; FAISAL</span></p>
					</div>
				</div>
			</div>
		</div>
		<aside id="fh5co-hero" class="js-fullheight">
			<div class="flexslider js-fullheight">
				<ul class="slides">
			   	<li style="background-image: url(images/MainImg4.jpg);">
			   		<div class="overlay-gradient"></div>
			   		<div class="container">
			   			<div class="col-md-8 col-md-offset-2 col-md-push-4 js-fullheight slider-text">
			   				<div class="slider-text-inner">
			   					<div class="desc">
			   						<span>On Aug. 07, 2017</span>
			   						<h2>Live Concert by " Daft Punk " <i class="icon-heart3"></i></h2>
			   						<p>Organized by: ONYX .. Hookah, Drinks and Many more waiting for you So,don't forget to join us .. Get yourself registered at WWW.ONYXnightClub.com</p>
			   					</div>
			   				</div>
			   			</div>
			   		</div>
			   	</li>
			   	<li style="background-image: url(images/MainImg5.jpg);">
			   		<div class="overlay-gradient"></div>
			   		<div class="container">
			   			<div class="col-md-8 col-md-offset-2 col-md-push-4 js-fullheight slider-text">
			   				<div class="slider-text-inner">
			   					<div class="desc">
			   						<span>September 2017</span>
				   					<h2>Marshmellow is on Tour Now</h2>
				   					<p>Buckle Up Boys and Girls, get yourself registered at WWW.SingSong.com</p>
				   				</div>
			   				</div>
			   			</div>
			   		</div>
			   	</li>
                </ul>
                </div>
    </aside>
    
        <div id="fh5co-about">
			<div class="container">
				<div class="row animate-box">
					<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
						<h2>Meet Our Staff</h2>
						<p>The Three Musketeers.</p>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-3 animate-box" data-animate-effect="fadeIn">
						<div class="fh5co-staff">
							<img class="img-responsive" src="images/hamza.jpg" alt="Free HTML5 Templates by freeHTML5.co">
							<h3>Hamza Saleem</h3>
							<strong class="role">CEO, Founder</strong>
							<p>Hi !! People calls me Hamza Motu. The Mastermind of this Project.</p>
							<ul class="fh5co-social-icons">
								<li><a href="#"><i class="icon-twitter2"></i></a></li>
								<li><a href="#"><i class="icon-facebook2"></i></a></li>
								<li><a href="#"><i class="icon-instagram"></i></a></li>
								<li><a href="#"><i class="icon-dribbble2"></i></a></li>
							</ul>
						</div>
					</div>&nbsp;&nbsp;
					<div class="col-sm-3 animate-box" data-animate-effect="fadeIn">
						<div class="fh5co-staff">
							<img class="img-responsive" src="images/ayub.jpg" alt="Free HTML5 Templates by freeHTML5.co">
							<h3>Ayub Idrees</h3>
							<strong class="role">Web Developer</strong>
							<p>Hi !! I'm a normal person like others and making this website.</p>
							<ul class="fh5co-social-icons">
								<li><a href="#"><i class="icon-twitter2"></i></a></li>
								<li><a href="#"><i class="icon-facebook2"></i></a></li>
								<li><a href="#"><i class="icon-instagram"></i></a></li>
								<li><a href="#"><i class="icon-dribbble2"></i></a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-3 animate-box" data-animate-effect="fadeIn">
						<div class="fh5co-staff">
							<img class="img-responsive" src="images/faisal.jpg" alt="Free HTML5 Templates by freeHTML5.co">
							<h3>Faisal Sheikh</h3>
							<strong class="role">System Analyst</strong>
							<p>Hi !! I'm Skilled and organized form of Sheikh.Currently doing Final Year project with my Friends.</p>
							<ul class="fh5co-social-icons">
								<li><a href="#"><i class="icon-twitter2"></i></a></li>
								<li><a href="#"><i class="icon-facebook2"></i></a></li>
								<li><a href="#"><i class="icon-instagram"></i></a></li>
								<li><a href="#"><i class="icon-dribbble2"></i></a></li>
							</ul>
						</div>
					</div>
                    <div class="col-sm-3 animate-box" data-animate-effect="fadeIn">
						<div class="fh5co-staff">
							<img class="img-responsive" src="images/user-3.jpg" alt="Free HTML5 Templates by freeHTML5.co">
							<h3>Unknown</h3>
							<strong class="role">Web Developer</strong>
							<p>Hi !! I'm Skilled and organized form of Sheikh.Currently doing Final Year project with my Friends.</p>
							<ul class="fh5co-social-icons">
								<li><a href="#"><i class="icon-twitter2"></i></a></li>
								<li><a href="#"><i class="icon-facebook2"></i></a></li>
								<li><a href="#"><i class="icon-instagram"></i></a></li>
								<li><a href="#"><i class="icon-dribbble2"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
    <?php
        $query = "SELECT COUNT(*) FROM user_info";
        $run = mysqli_query($conn,$query);
        $row = mysqli_fetch_array($run);
        $usrCount = $row['COUNT(*)'];
    
        $que = "SELECT COUNT(*) FROM news";
        $result = mysqli_query($conn,$que);
        $row = mysqli_fetch_array($result);
        $newsCount = $row['COUNT(*)'];
    
        $q = "SELECT COUNT(*) FROM song";
        $re = mysqli_query($conn,$q);
        $row = mysqli_fetch_array($re);
        $songCount = $row['COUNT(*)'];
    ?>
        <div class="fh5co-counters" style="background-image: url(images/hero.jpg);" data-stellar-background-ratio="0.5" id="counter-animate">
			<div class="fh5co-narrow-content animate-box">
				<div class="row" >
					<div class="col-md-4 text-center">
						  <span class="fh5co-counter js-counter" data-from="0" data-to="<?php echo $usrCount; ?>" data-speed="5000" data-refresh-interval="50"></span>
						<span class="fh5co-counter-label">Users</span>
					</div>
					<div class="col-md-4 text-center">
						<span class="fh5co-counter js-counter" data-from="0" data-to="<?php echo $newsCount; ?>" data-speed="5000" data-refresh-interval="50"></span>
						<span class="fh5co-counter-label">Gig News</span>
					</div>
					<div class="col-md-4 text-center">
						<span class="fh5co-counter js-counter" data-from="0" data-to="<?php echo $songCount; ?>" data-speed="5000" data-refresh-interval="50"></span>
						<span class="fh5co-counter-label">Songs</span>
					</div>
				</div>
			</div>
		</div>
    
</div>

<?php include 'footer.php';?>
    
<?php include 'header.php';?>
<?php include 'includes/db_connection.php'; ?>
    <div id="fh5co-page">
        <div id="fh5co-intro-section">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 animate-box text-center">
						<h2 class="intro-heading">Melodic "A Music Site"</h2>
						<p><span>Created with <i class="icon-heart3"></i> by HAMZA , AYUB &amp; FAISAL  </span></p>
                        <h1 class="text-center" style="color:#539DDB;">Sign Up</h1>
					</div>
				</div>
			</div>
		</div>

        <div id="fh5co-contact-section" ng-app="">
			<div class="container">
				<div class="row">
                    <?php
                        if(isset($_GET['success'])){
                            $success = $_GET['success'];
                            echo $success;
                        }
                        if(isset($_GET['error'])){
                            $error = $_GET['error'];
                            echo $error;
                        }
                    ?>
					<div class="col-md-7 col-md-push-1 animate-box">
						<div class="row">
                            <form name="myForm" action="includes/signup.php" enctype="multipart/form-data" method="post" novalidate>
                                 <div class="col-md-7">
								    <div class="form-group">
									   <input type="text" class="form-control" placeholder="Username" id="Uname" name="Uname" ng-model="Uname" required>  <span ng-show="myForm.Uname.$touched && myForm.Uname.$invalid">*Name is Required</span> 
								    </div>
							     </div><br/>&nbsp;
							     <div class="col-md-7">
								    <div class="form-group">
									   <input type="text" class="form-control" placeholder="Your Name" id="name" name="name" ng-model="name" required>  <span ng-show="myForm.name.$touched && myForm.name.$invalid">*Name is Required</span> 
								    </div>
							     </div><br/>&nbsp;
                                <div class="col-md-7">
								    <div class="form-group">									
                                        <input type="password" class="form-control" placeholder="Set Password" id="passW" name="passW" ng-model="passW" required ng-pattern="/^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$/">
                                            <span ng-show="myForm.passW.$touched && myForm.passW.$invalid">*Password should be atleast 8 characters long and should contain one number,one character and one special character</span>  
								    </div>
							     </div><br/><br/>
							     <div class="col-md-7">
								    <div class="form-group">
									   <input type="email" class="form-control" placeholder="Email" id="email" name="email" ng-model="email" required>
                                        <span ng-show="myForm.email.$touched && myForm.email.$invalid">*Email is Required</span> 
								    </div>
							     </div><br/>
                                 <div class="col-md-7">
								    <div class="form-group">
									   <input type="number" class="form-control" placeholder="Mobile Number" id="M_no" name="M_no" ng-model="mobileNumber" ng-minlength="11" ng-maxlength="11" required>
                                        <span class="help-block" ng-show="myForm.M_no.$error.required || myForm.M_no.$error.number">*Valid phone number is required</span>
                                        <span class="help-block" ng-show="((myForm.M_no.$error.minlength || myForm.M_no.$error.maxlength) && myForm.M_no.$dirty) ">*Phone number should be 11 digits</span>
								    </div>
							     </div><br/><br/><br/><br/>
                                 <div class="col-md-7">
								    <div class="form-group">
									   <input type="text" class="form-control" placeholder="Address" id="add" name="add">
								    </div>
							     </div><br/><br/>
                                 <div class="col-md-12">
                                    <div class="form-group">
                                        Gender: <br/>
                                        <input type="radio" name="optGender" id="optGender" group="optGender" value="Male" >Male<br/> 
                                        <input type="radio" name="optGender" id="optGender" group="optGender" value="Female" >Female<br/> 
                                        <input type="radio" name="optGender" id="optGender" group="optGender" value="Others" >Others
                                    </div>
							     </div><br/><br/>    
                                 <div class="col-md-12">
                                    <div class="form-group">
                                        <p>Date of Birth:</p>
                                        <input type="date" id="birthday" name="birthday" min="1917-01-01" max="2015-01-01" ng-model="birthday" required><br/> 
                                          <span ng-show="myForm.birthday.$touched"></span>     
                                          <span class="help-block" ng-show="myForm.birthday.$error.required">*Date of Birth is Required!</span>
                                          <span class="help-block" ng-show="myForm.birthday.$error.date">*Not a valid date!</span>
                                    </div>
				                 </div><br/><br/>
                    
                                 <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="file" value="Choose a file to Upload" onchange="readURL(this);" name="image" id="image" class="inputfile" />
                                        <img class="img-song" id="blah" src="#" alt="your image" >
                                        <br/>&nbsp;
                                    </div>
                                </div>
                                
							     <div class="col-md-12">
								    <div class="form-group">
									   <input type="submit" name="submit" value="Sign Up" ng-disabled="myForm.$invalid" class="btn btn-primary">  
								    </div>
							     </div>
                                
                            </form>
						</div>
					</div>
				</div>
			</div>

           
<?php include 'footer.php';?>
<script>
    function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result)
                        .width(150)
                        .height(200);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
</script>             